//
//  ViewController.swift
//  TableViewDetail_Swift
//
//  Created by PRIYA GONGAL on 30/07/19.
//  Copyright © 2019 Priya Gongal. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var array1 = ["AAA","BBB","CCC"]
    var array2 = ["aaa","bbb","ccc"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array2.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell")
        cell?.textLabel?.text = array1[indexPath.row]
        cell?.detailTextLabel?.text = array2[indexPath.row]
        
        return cell!
    }
}

